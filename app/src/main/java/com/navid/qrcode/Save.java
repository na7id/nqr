package com.navid.qrcode;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.ArrayList;

public class Save extends Activity
{

    ArrayList<String> list;

    public void onCreate(Bundle saveInstanceState)
    {
        super.onCreate(saveInstanceState);
        setContentView(R.layout.activity_save);

        ListView history=(ListView)findViewById(R.id.listView);


        list = new ArrayList<String>();

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        Gson gson = new Gson();
        String json = prefs.getString("Navid", null);
        Type type = new TypeToken<ArrayList<String>>() {}.getType();

        if(gson.fromJson(json, type)!=null)
            list = gson.fromJson(json, type);


        ArrayAdapter<String> arrayAdapter =
                new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, list);

        history.setAdapter(arrayAdapter);


    }





}
