package com.navid.qrcode;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class SaveItems {

    public static void saveNewItem(Context act, String text)
    {
        ArrayList<String> list = new ArrayList<String>();

        list.clear();

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(act);
        Gson gson = new Gson();
        String json = prefs.getString("Navid", null);
        Type type = new TypeToken<ArrayList<String>>() {
        }.getType();

        if(gson.fromJson(json, type)==null) {
            saveInit(act);

            list.add(text);
            SharedPreferences prefs2 = PreferenceManager.getDefaultSharedPreferences(act);
            SharedPreferences.Editor editor = prefs2.edit();
            Gson gson3 = new Gson();
            String json3 = gson3.toJson(list);
            editor.putString("Navid", json3);
            editor.apply();
        }else
        {
            list = (gson.fromJson(json, type));
            list.add(text);
            SharedPreferences prefs2 = PreferenceManager.getDefaultSharedPreferences(act);
            SharedPreferences.Editor editor = prefs2.edit();
            Gson gson2 = new Gson();
            String json2 = gson2.toJson(list);
            editor.putString("Navid", json2);
            editor.apply();
        }




    }

    public static void saveInit(Context act) {
        ArrayList<String> list = new ArrayList<String>();
        list.add("first of list");
        String key = "Navid";
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(act);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(key, json);
        editor.apply();
    }
}
