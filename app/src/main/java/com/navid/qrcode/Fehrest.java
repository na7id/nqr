package com.navid.qrcode;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.io.Serializable;

public class Fehrest extends Activity implements Serializable {

    Button make;
    Button camera;
    Button history;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fehrest);

        init();

        history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentAnimalList = new Intent(getApplicationContext(), Save.class);
                startActivity(intentAnimalList);
            }
        });
        make.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             startActivity(new Intent(getApplicationContext(),MakeQr.class));
            }
        });
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Show the Camera to Scan
                IntentIntegrator ii = new IntentIntegrator(Fehrest.this);
                ii.setDesiredBarcodeFormats(ii.QR_CODE_TYPES);
                ii.setCameraId(0);
                ii.setOrientationLocked(false);
                ii.setPrompt("درحال اسکن کردن...");
                ii.initiateScan();
            }
        });
    }

    private void init() {

        make = findViewById(R.id.make);
        camera = findViewById(R.id.camera);
        history = findViewById(R.id.history);


    }



//
//    public ArrayList<String> getArrayList(String key) {
//        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
//        Gson gson = new Gson();
//        String json = prefs.getString(key, null);
//        Type type = new TypeToken<ArrayList<String>>() {
//        }.getType();
//        return gson.fromJson(json, type);
//    }
//
//    public void makeLastArray() {
//        if(getArrayList("key")!=null)
//        tempList.addAll(getArrayList("key"));
//    }
//
//

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        final IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null && result.getContents() != null) {
            SaveItems.saveNewItem(getApplicationContext(),result.getContents().toString());
            Toast.makeText(getApplicationContext(), result.getContents(), Toast.LENGTH_SHORT).show();
            new AlertDialog.Builder(Fehrest.this)
                    .setTitle("Scan Result")
                    .setMessage(result.getContents())
                    .setPositiveButton("copy", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ClipboardManager manager = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                            ClipData data = ClipData.newPlainText("Result", result.getContents());
                            manager.setPrimaryClip(data);
                        }
                    }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            }).create().show();
        }

        super.onActivityResult(requestCode, resultCode, data);

    }
}
